category	text
1	"Comparing their performance is difficult since both management systems are extremely useful and the core differences underlie their basic operations and initial approach.  "
1	"Both are open-source and easily available, as well as both systems offer commercial versions with tons of additional features. "
1	"Another issue with the latter one is the owner’s focus on MariaDB development along with refuse to accept community patches and to provide sustainability plan.  "
1	"These factors have resulted in a standstill, though this solution is still the go-to solution for multiple companies worldwide. "
1	"Comparing the speed, developers note that the latter one lacks speed and experience difficulties with large data volumes, so it’ll be a better choice for companies with smaller databases and looking for a more general solution.  "
1	"While this is one of the advantages of non-relational databases the ability to cope with large and unstructured amounts of data. "
1	"To answer the main question: “when to use MongoDB instead of MySQL?” you need to take into account your project requirements and further goals.  "
1	"According to GitHub’s annual Octoverse report, Java and Python are the second and third most popular languages for the fourth year in a row.  "
1	"According to the same story, Python is one of the top ten fastest growing languages.  "
1	"Most of the other fast-gainers are new languages, while Python has been around longer than Java. "
1	"Java and Python have many similarities.  "
1	"Both languages have strong cross-platform support and extensive standard libraries.  "
1	"They both treat (nearly) everything as objects.  "
1	"Both languages compile to bytecode, but Python is (usually) compiled at runtime.  "
1	"They are both members of the Algol family, although Python deviates further from C/C++ than Java does. "
1	"Python has a more unified support model than Java for the first time, and open source developers are focusing their efforts on the latest version of the language. I have to give Python the edge here. "
1	"Whether Python’s dynamic typing is better than Java’s static approach is subjective.  "
1	"The debate between the two models predates both of them, and it’s a question of what’s best for you and your team. "
1	"After working on large projects in both languages, I feel secure saying that Python’s syntax is more concise than Java’s. It’s easier to get up and running quickly with a new project in Python than it is in Java. Python wins again. "
1	"Performance is where Java has a substantial advantage over Python. "
1	"Java’s just-in-time compilation gives it an advantage over Python’s interpreted performance.  "
1	"While neither language is suitable for latency-sensitive applications, Java is still a great deal faster than Python. "
1	"All things considered, Python’s advantages outweigh the disadvantages. If you’re not already considering it, give it another look. "
1	"The tests show that multi-model databases can compete with single model databases.  "
1	"It is faster at single document reads but couldn’t compete when it comes to aggregations or 2nd neighbors selections.  "
1	"Note: the shortest path query was not tested for some solutions as it would have to be implemented completely on the client side. "
1	"The aggregation in ArangoDB is efficient which defines the baseline, only an explicit table column age in PostgreSQL is much faster. "
1	"As PostgreSQL offers the JSON data type as well, you might want to check the performance is beyond everything you want to accept.  "
1	"All other databases are much slower than ArangoDB, from factor x2.5 in MongoDB to x20 in case of OrientDB. "
1	"At least Neo4j and OrientDB can’t stand out in this test despite it’s a simple graph traversal.  "
1	"ArangoDB is really fast using just 464ms in AVG, no graph database comes close.  "
1	"That’s because lookups in a graph of a known length is faster with an Index lookup than using outbound links in every vertex. "
1	"The test results show that ArangoDB can compete with leading databases in their fields and also with the other multi-model database, OrientDB.  "
1	"Memory is our pain point when compared to other systems and it will be addressed in the next major release.  "
1	"With a flexible data model, you can use a multi-model database in many different situation without the need to learn new technologies. A short selection of real life tasks has been given here. "
1	"much faster as MRF loss is the slowest part of the algorithm. "
1	"`dendrogram` more elegantly handles extremely large datasets by simply flipping to a horizontal configuration. "
1	"the default and requires less GPU memory but is less accurate then brute. "
1	"One of the fastest Python frameworks available "
1	"These and similar large projects are supported much more actively by a larger number of contributors. "
1	"Tron Virtual Machine (TVM) allow anyone to develop decentralized applications (DAPPs) for themselves or their communities with smart contracts thereby making decentralized crowdfunding and token issuance easier than ever.\r "
1	"**Please note**: This method is less efficient than `observeNetworkConnectivity(context)` method, because in default observing strategy, it opens socket connection with remote host (default is www.google.com) every two seconds with two seconds of timeout and consumes data transfer. "
1	"Increasing this number will slightly improve the performance, but also cause training to be less stable. "
1	"A key-value store is the basis for more complex systems such as a document store, and in some cases, a graph database. "
1	"If you know of a better method, let me know (or even better open a pull request)! "
1	"This utility will help to pull messages from Kafka using Spark Streaming and have better handling of the Kafka Offsets and handle failures. "
1	"This is helpful for more sophisticated visualizations in which configuration is meaningful, e.g. "
1	"Included below are hyper parameters to get equivalent or better results to those included in the original paper. "
1	"Realm is faster than even raw SQLite on common operations while maintaining an extremely rich feature set. "
1	"The Completer API is a much more powerful way to integrate with YCM and it. "
1	"Open Source means: No backdoors, control is better than trust. "
1	"BlurKit is faster than other blurring libraries due to a number of bitmap retrieval and drawing optimizations. "
1	"This is faster than `Affine`. "
1	"The library is faster than other libraries on most of the transformations. "
1	"KaTeX is faster than MathJax on mobile environment, but MathJax supports more features and is much more beautiful. "
1	"It is bigger than your average tutorial and smaller than an actual book. "
1	"There are many implementations of sorts in the Java standard library that are much better for performance reasons. "
1	"There are many ways to create rounded corners in android, but this is the fastest and best one that I know of because it: "
1	"Download best format available but no better than 480p "
1	"Remote calls are usually slower and less reliable than local calls so it is helpful to distinguish RPC calls from local calls. "
1	"the configuration files are now much smaller than before. "
1	"It is more complex to implement write-behind than it is to implement cache-aside or write-through. "
1	"Here is a slightly more complex example that launches a registry on port 5000, using an Amazon S3 bucket to store images with a custom path, and enables the search endpoint. "
1	"Sharding adds more hardware and additional complexity. "
1	"Fail-over adds more hardware and additional complexity. "
1	"the queue size can become larger than memory. "
1	"Putting it in the hands of junior developers may cause more harm than good. "
1	"Adding a classfier to trained with conditions and constraint G works faster and better than appending conditions to images for D training. "
1	"It provides a simpler and more interactive SQL interface for stream processing on Kafka if compared with other solutions. "
1	"'git diff' much more useful (compared to splitting a tarball, which is essentially a big binary blob). "
1	"Regex require less boilerplate when compared to Python's standard `re` module. "
1	"Compared to the ROM bootloader that esptool.py talks to, a running firmware uses more of the chip\'s pins to access the SPI flash. "
1	"Specialized implementations offer better performance compared to standard APIs. "
1	"`dendrogram` more elegantly handles extremely large datasets by simply flipping to a horizontal configuration. "
1	"Performance with the Completer API is better since Python executes faster than other alternatives. "
1	"The library is faster than other libraries on most of the transformations. "
1	"The default QRNN models can be far faster than the cuDNN LSTM model, with the speed-ups depending on how much of a bottleneck the RNN is. "
0	"The folder would likely have many subfolders, and they would all copy in the same transaction."
0	"We consider that each software unit has a quantifiable degree of portability to a particular environment or class of environments, based on the cost of porting. "
0	"Expo is a set of tools, libraries, and services that let you build native iOS and Android apps by writing JavaScript."
0	"Recently, the Java programming language and runtime environment has made it possible to have programs that run on any operating system that supports the Java standard (from Sun Microsystems) without any porting work. "
0	"Portability is not a binary attribute. "
0	"With Robolectric, your tests run in a simulated Android environment inside a JVM, without the overhead of an emulator."
0	"Different from hardware reliability since it doesn’t age, wear out, rust, deform or crack!"
0	"Try hard to make tests that run fast."
0	"Most bugs arise from mistakes and errors made in either a program's source code or its design, or in components and operating systems used by such programs. "
0	"Hacker News client with a focus on reliability and robustness."
0	"A software bug is an error, flaw, failure or fault in a computer program or system that causes it to produce an incorrect or unexpected result, or to behave in unintended ways. "
0	"These activities will take additional time to complete if the code is not easy to manage in the first place. An application with these qualities will require increased programming effort: Poor Code Quality, Source Code Defects, Undetected Vulnerabilities, Excessive Technical Complexity, Large Systems, Poorly Documented Systems, Excessive Dead Code."
0	"Maintainability increases the reliability, efficiency or safety of the software. "
0	"The Irreproducibility Of Bugs In Large-Scale Production Systems."
0	"Each test must be able to run alone, and also within the test suite, regardless of the order that they are called. "
0	"Improvements and bug fixes related to the taint analysis."
0	"Fundamentally, it's what happens when you ask a software engineer to design an operations function."
0	"Maintains full records eg to: trace any transaction item, provide a full audit trail, provide a full record of all balances and transactions."
0	"Checking that system responds correctly to all kinds of inputs."
0	"Welcome to the wonderful world of automated testing, specifically unit testing. "
0	"Software maintenance is a part of Software Development Life Cycle. "
0	"Your code will be easier to change, unit test and reuse in other contexts."
0	"iOS in-app bug reporting for developers and testers, with annotated screenshots and the console log."
0	"Fundamentally, it's what happens when you ask a software engineer to design an operations function."
0	"Support to fix bugs in constructors currently is under testing."
0	"The primary focus is on the exploration of energy efficient techniques and architectures for cognitive computing and machine learning, particularly for applications and systems running at the edge."
0	"Scala macros for compile-time generation of ultra-fast JSON codecs."
0	"Fast JSON parser LINQ-style queries Case classes can be used to extract values from parsed JSON Diff & merge."
0	"Code efficiency is directly linked with algorithmic efficiency and the speed of runtime execution for software."
0	"We use GitHub issues for tracking requests and bugs, please see TensorFlow Discuss for general questions and discussion, and please direct specific questions to Stack Overflow."
0	"Indexes can be created using one or more columns of a database table, providing the basis for both rapid random lookups and efficient access of ordered records."
0	"A simple Python program may not cause many problems when it comes to memory, but memory utilization becomes critical on high memory consuming projects."
0	"A computer power supply does not send data about power consumption to the motherboard, nor does it measure power consumption."
0	"Varvy - Page speed optimization."
0	"If a query searches multiple fields, create a compound index. Scanning an index is much faster than scanning a collection. "
0	"This reduces recompilation time a lot."
0	"Algorithm to update DOM Precise data-binding, which is faster than virtual DOM Virtual DOM differentiation, which requires manually managed attributes for complicated DOM."
0	"IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
0	"For the purposes of this definition, "control" means (i) the power, direct or indirect, to cause the direction or management of such entity, whether by contract or otherwise, or (ii) ownership of fifty percent (50%) or more of the outstanding shares, or (iii) beneficial ownership of such entity."
0	"For license information regarding the FFHQ dataset, please refer to the  Flickr-Faces-HQ repository ."
0	"However, in accepting such obligations, You may act only on Your own behalf and on Your sole responsibility, not on behalf of any other Contributor, and only if You agree to indemnify, defend, and hold each Contributor harmless for any liability incurred by, or claims asserted against, such Contributor by reason of your accepting any such warranty or additional liability."
0	"a copyright notice, ii."
0	"Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software."
0	"See the License for the specific language governing permissions and limitations under the License."
0	"You may reproduce and distribute copies of the Work or Derivative Works thereof in any medium, with or without modifications, and in Source or Object form, provided that You meet the following conditions:  (a) You must give any other recipients of the Work or Derivative Works a copy of this License, and  (b) You must cause any modified files to carry prominent notices stating that You changed the files, and  (c) You must retain, in the Source form of any Derivative Works that You distribute, all copyright, patent, trademark, and attribution notices from the Source form of the Work, excluding those notices that do not pertain to any part of the Derivative Works, and  (d) If the Work includes a "NOTICE" text file as part of its distribution, then any Derivative Works that You distribute must include a readable copy of the attribution notices contained within such NOTICE file, excluding those notices that do not pertain to any part of the Derivative Works, in at least one of the following places: within a NOTICE text file distributed as part of the Derivative Works, within the Source form or documentation, if provided along with the Derivative Works, or, within a display generated by the Derivative Works, if and wherever such third-party notices normally appear."
0	"We also recommend that a file or class name and description of purpose be included on the same "printed page" as the copyright notice for easier identification within third-party archives."
0	"Further, Affirmer disclaims responsibility for obtaining any necessary consents, permissions or other rights required for any use of the Work."
0	"Copyright (c) 2018 omi  4399.omi@gmail.com"
0	"IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
0	"g. License Elements means the license attributes listed in the name of a Creative Commons Public License."
0	"Offer from the Licensor -- Licensed Material."
0	"You may add Your own copyright statement to Your modifications and may provide additional or different license terms and conditions for use, reproduction, or distribution of Your modifications, or for any such Derivative Works as a whole, provided Your use, reproduction, and distribution of the Work otherwise complies with the conditions stated in this License."
0	"Accepting Warranty or Additional Liability."
0	"To the extent this Public License may be interpreted as a contract, You are granted the Licensed Rights in consideration of Your acceptance of these terms and conditions, and the Licensor grants You such rights in consideration of benefits the Licensor receives from making the Licensed Material available under these terms and conditions."
0	"Licensing conditions (BSD-style) can be found in LICENSE.txt."
0	""Contributor" shall mean Licensor and any individual or Legal Entity on behalf of whom a Contribution has been received by Licensor and subsequently incorporated within the Work."
0	"Limitation of Liability."
0	"License Copyright 2013-2018 Twitter, Inc."
0	"MIT License  https://en.wikipedia.org/wiki/MIT_License"
0	"All files in the directories active_projects and old_projects, which by and large generate the visuals for 3b1b videos, are copyright 3Blue1Brown."
0	"If not, see <http://www.gnu.org/licenses/>."
0	"Because this is my personal repository, the license you receive to my code and resources is from me and not my employer (Facebook)."
0	"A Fast, Extensible Progress Bar for Python and CLI."
0	"All of the code in this repository works out-of-the-box with CPU, GPU, and Cloud TPU."
0	"Ease of use: CXF is designed to be intuitive and easy to use. There are simple APIs to quickly build code-first services, Maven plug-ins to make tooling integration easy, JAX-WS API support, Spring 2.x XML support to make configuration a snap, and much more."
0	"XGBoost provides a parallel tree boosting (also known as GBDT, GBM) that solve many data science problems in a fast and accurate way. "
0	"HBase is a distributed, scalable, big data store."
0	"Various helpers to pass trusted data to untrusted environments."
0	"Simple, Pythonic, text processing--Sentiment analysis, part-of-speech tagging, noun phrase extraction, translation, and more."
0	"Python implementation of cover trees, near-drop-in replacement for scipy.spatial.kdtree."
0	"Powerline is a statusline plugin for vim, and provides statuslines and prompts for several other applications, including zsh, bash, tmux, IPython, Awesome and Qtile."
0	"A truly minimal Markdown editor featuring seamless live preview."
0	"Features - This library manages everything by itself opening and closing connections, commits, cursors..."
0	"Content API for 'headless' sites with de-coupled front-end."
0	"A command line HTTP client, a user-friendly cURL replacement."
0	"The most popular mind-mapping tool on the planet."
0	"Spinnaker is an open source, multi-cloud continuous delivery platform for releasing software changes with high velocity and confidence."
0	"Easy & Flexible Alerting With ElasticSearch."
0	"Distributed Closure Execution"
0	"The purpose of Luigi is to address all the plumbing typically associated with long-running batch processes. You want to chain many tasks, automate them, and failures will happen. These tasks can be anything, but are typically long running things like Hadoop jobs, dumping data to/from databases, running machine learning algorithms, or anything else."
0	"A computer algebra system written in pure Python."
0	"Distributed & Persistent Redis Clone built with Scala & Akka."
0	"Python Library for Probabilistic Graphical Models."
0	"Glances an Eye on your system. A top/htop alternative for GNU/Linux, BSD, Mac OS and Windows operating systems."
0	"Tokenization For sentence-level tasks (or sentence-pair) tasks, tokenization is very simple."
0	"ML-Ensemble – high performance ensemble learning."
0	"Easy, automatic and powerful information display system."
0	"Navigational Components: breadcrumb, slider, search field, pagination, slider, tags, icons."
0	"Within each large band of color on the map, we placed several polygons filled with each map color (‘outliers’)."
0	"Easy, automatic and powerful information display system."
0	"Users are satisfied when an interface is user-centered – when their goals, mental models, tasks and requirements are all met. "
0	"The first goal of usability is efficiency and effectiveness while aesthetic value comes after a product has proven to be usable."
0	"Users are satisfied when an interface is user-centered – when their goals, mental models, tasks and requirements are all met. "
0	"Create consistency and use common UI elements. By using common elements in your UI, users feel more comfortable and are able to get things done more quickly. "
0	"Usability Evaluation assitant to facilitate doing experiments with end users."
0	"The combination of analysis, design and evaluation all approached starting from the user’s point of view creates usable products."
0	"Syntax Highlighting - Language specific syntax highlighting with multiple color schemes."
0	"Different sizes, fonts, and arrangement of the text to help increase scanability, legibility and readability."
0	"Perception is what the user see's and feels when they are using a HCI. To help us see, feel and use a HCI designers use colors, patterns and objects. "
0	"Make sure that the system communicates what’s happening.  Always inform your users of location, actions, changes in state, or errors. The use of various UI elements to communicate status and, if necessary, next steps can reduce frustration for your user. "
0	"Easy to use, with: full access to all system functions, subject to individual user security profile(s), straight forward system navigation, short cut keys, pull down menus, scroll backwards and forwards, tailorable menus, screens, reports, multi tasking ie multiple applications / windows open at a time, on-screen prompts and messages that are clear and helpful eg requesting input, identifying input validation errors or processing errors"
0	"Navigational Components: breadcrumb, slider, search field, pagination, slider, tags, icons."
0	"Efficiency: Once users have learned the design, how quickly can they perform tasks?"
0	"It create actions in response to user interactions."
0	"Simple interface to facilitate user adoption."
0	"Successful software projects please customers, streamline processes, or otherwise add value to your business."
0	"Iterative design is a design methodology based on a cyclic process of prototyping, testing, analyzing, and refining a product or process.Based on the results of testing the most recent iteration of a design, changes and refinements are made. "
0	"If you are not familiar with pull requests, review the [pull request docs](https://help.github.com/articles/using-pull-requests/)."
0	"We're happy to meet new contributors."
0	"Submit a pull request."
0	"Submit a pull request."
0	"Glide is open source."
0	"Contributing See guidelines for contributing here."
0	"Your help is very valuable to make the package better for everyone."
0	"Thanks to all our contributing authors, including (in chronological order):"
0	"## Pull Requests."
0	"Related Projects List of known projects that fork or use changes from ungoogled-chromium"
