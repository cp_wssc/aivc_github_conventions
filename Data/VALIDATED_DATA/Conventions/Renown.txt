* view user followers, following, and organizations
*note: to scrape a private user\'s media you must be an approved follower.
It provides a high-level interface for drawing attractive statistical graphics.
    newsblur\'s followers are largely technical.
since the target audience is developers, i opted to leave some parts a little less user-friendly, such as the `settings.py` config system.
\'path\' has a very attractive menu sitting on the left bottom corner of the screen.
Chrome\'s vast user base makes it the most attractive target for threat actors and security researchers.
## audience of django-shop users
  * hall of fame of the best individuals that lived in the population
if you enjoy octoprint, please consider becoming a regular supporter!**
* 25 ready-made social badges using brand colors
The brand name ERPNext and the logo are trademarks of Frappe Technologies Pvt.
justifications based on renown and public opinion ["renown" worth] while all orders of justifications involve arguments designed to gain public support, the standard for judgment and evaluation of arguments in the other orders is not the extent of public knowledge or renown itself.
however, both cases involve attempts at gaining media attention or influencing the impressions of an audience who might not know the issues in-depth.
more significantly, both cases involve denunciations of these sorts of "publicity" moves.
the clavey dispute, advocates for both sides had at their disposal a well-developed set of tools (used in political disputes of all sorts in the united states) for generating public and media attention, such as a fax network for press releases, slick bumper stickers (the opposition   s read simply: "save the clavey"), guest editorials sent out to newspapers for consideration, slide shows, newsletters and mass mailings, endorsement campaigns and announcements, and a distinctly american form of pamphlet, the "alert".
interviews with key environmental leaders in the clavey dispute reveal they were explicitly concerned with making the clavey a well-known issue, particularly among environmentally minded citizens in san francisco and nationwide, and among key elected officials such as national democratic congressional representatives who might endorse the opposition campaign.
trpt worked with national environmental groups including american rivers, which publishes an annual list of "the ten most endangered rivers" in the united states and generates a great deal of national media coverage.
the head of trpt, johanna thomas, said the national groups acted "kind of like a public relations firm for the river, because they had the ability to put out materials and reach a huge audience, much like advertisers do".
this advertisement for the river then paid crucial dividends in the form of pressure on the dam proponents to recognize that not just the politicians were against the dam but also masses of people from throughout the state and nation, many of whom wrote letters to government calling for a halt to the project.
she contends, "i think it was very threatening when [tid] saw that the clavey was getting this kind of attention".
the denunciation of these publicity efforts by the dam proponents also suggests the relevance and power of opinion claims.
mills attempts to play down the national renown of the clavey by pointing out that most of the people who wrote opposition letters were "outsiders" who would never see the river and had no "stake" in the dispute.
he characterized them as professional adversaries of any dams, who for that reason should have no weight in local decision-making.
mills goes further to argue against judging the project in terms of opinion at all; he believes opinion evaluations are dangerous because opinion is so fickle and so easily manipulated by "public affairs gurus" and "spin doctors" who know how to "push buttons" on "whatever   s popular right now".
another project supporter, shirley campbell, also acknowledged the importance of opinion on a local level.
she helped start the wise use group tucare partly because she wanted to counter the loud publicity from the environmentalists.
thus, the clavey dispute in the local area took on the appearance of a "public opinion war" because both sides effectively utilized the many tools available to them.
while public opinion is also at stake in the somport case, the goal of generating media attention and public notoriety is less explicitly pursued by the participants, and the mechanisms for renown are much less developed.
the famous singer of protest songs, renaud, made headlines when he came out against the tunnel, and he has continued to make dramatic public statements denouncing the tunnel.
also, there has been quite a lot of national and international press coverage of the large protests against the project.
but there has been some reaction against the press treatment of the project, partly because the press were considered "parisian" and not local.
finally, the minister of the environment, brice lalonde, has been denounced by opponents as being overly concerned with managing his image through his prominent role in the dispute.
attracting the attention and influencing the opinion of politicians is another area of comparative difference.
this is an essential goal of both sides of the clavey conflict, and both sides were active in lobbying both directly and indirectly.
the opposition coalitions actively sought the endorsement of key legislators, and even took many legislators on rafting trips with the goal of letting them personally "experience" the clavey.
much attention was given by the media to a visit by senator barbara boxer to the region, when she came out against the dam.
again, the denunciations of these renown tactics also reveal the significance of public attention in contributing to the fate of the project.
activists in the clavey case even developed ingenious methods for informing the public and prompting "ordinary citizens" to write to their legislators.
for example, rafting companies active in fighting the dam had their guides talk to the customers about the dispute and (when they felt they had convinced them) ask them to write letters to their representatives and to ferc.
but also the idea of lobbying     trying to influence legislators outside of public settings or even publicly announcing endorsements of any one side in the dispute     would rarely be considered legitimate in france, while it is often (but not always) considered a legitimate tactic in the united states.
worth is nothing but the result of other people's opinion in the world of renown.
the measurement of people's worth depends on conventional signs of public esteem.
this kind of worth is based on nothing other than the number of individuals who grant their recognition.
it is hence entirely unrelated to the realm of personal dependencies and it is not linked to the person's self-esteem.
for this reason, disputes may arise when a gap between self-esteem and recognition by others comes to light: in this world, other people's recognition is reality.
as a guide we used a training book in public relations, principes et techniques des relations publiques.
in this world, relevant persons are well-known personalities, stars, opinion leaders, journalists.
they are worthy and great when they are famous, recognized, successful, or convincing.
the current objects in this world are trademarks, badges, message transmitters and receivers, press releases and booklets.
the right way of making relations is, then, to influence, to identify oneself to somebody, to appeal to or to speak about somebody, or to gossip and spread rumors.
the reality of public opinion (higher common principle)\ others (the), public (the, at large).
\nfame (state of worthiness) reputed,\nrecognized, visible,\nsuccess (to have),\ndistinguish (oneself),\npersuasive, attention getting.
\nthe desire to be recognized (digni1)\nself-love, respect (desire for).
\nstars and their fans (subjects) personality (a famous), opinion leader, spokesperson,\nrelay, journalist,\npublic relations agent.
\nnames in the media (objects) brand,\nmessage, sender,\nreceiver, campaign,\npublic relation, press,\ninterview, bulletin,\nmedium, brochure,\nmailing, badge,\naudiovisual,\natmosphere, setting.
\ngiving up secrets (investment) reveal.
\nbeing recognized and identifying\n(reiation of worth)\nidentification,\nstrength.
persuasion\n(relationships)\ninfluence, convince,\nsensitize, attract,\nseduce, hook,\npenetrate, capture,\nlaunch, emit,\ncirculate (cause to),\npropagate, \norient, amplify, talk about, cite.
\nthe public image (figures) audience,\ntarget, positioning.
\npresentation of the event (test)\ndemonstration, press conference,\ninauguration,\nopen house (demonstration).
\nthe judgment of public opinion (judgmfnt}\nrumor, unconfirmed report, fashion,\nstanding, sensation,\nrepercussion, proper proportions (reduce to),\nmeasure (the audience).
\nthe evidence of success (evidence) known.
\nindifference and banality (the fau)\nhidden,\nindifference (encounter), banal, forgotten, fuzzy image, deteriorated,\nfaded, lost.
the world of fame\nunlike the domestic world, but similar in this respect to the market world, the world of public opinion places little value on memory.
this is the feature andy warhol was alluding to in his famous announcement that a world is coming in which "everyone will have fifteen minutes of fame".
in the world of fame, there are few things that can consolidate and stabilize the relation between worth, which comes exclusively from the opinion of others, and the bearer of worth, an entity (person or thing) that is not qualified by properties scribed in its being in a lasting way.
this nonessentialist and purely relational character of the worth of fame may be precisely what has encouraged its adoption as a universal standard of measure by the schools of thought within the social sciences that like to highlight the structural and relativist properties of the social world.
in the world of fame, people may impose an order on beings and reach agreement in a just world by taking only the opinion of others into account.
it is opinion that establishes equivalence, and the worth of each being depends on the opinion of others: to a large extent, the reactions "of public opinion determine success".
persons are relevant inasmuch as they form a public whose "opinion prevails," a public "that creates public opinion" and thereby constitutes the only "true" reality: "isn't an opinion also a reality?"
fame establishes worth.
in the world of public opinion, worthy beings are the ones that distinguish themselves, are visible, famous, recognized: their visibility depends on their more or less attention-getting, persuasive, informative character.
let us note that, in this world, the qualifiers of worth are applied without distinction to persons and to other beings, even though, as in the other worlds, only persons can reach the highest state.
people are all capable of reaching that state because they share the property of being moved by self-love.
selflove is what creates their dignity as human beings.
they have a common desire to be recognized, a common craving for respect.
in the same way, "questioned by someone from outside his own company ... , the participant wants to be able to explain what his own role is, and to be respected everywhere, since part of the reputation of the company for which he works reflects back on him".
since worth derives solely from opinion, the other qualities, and in particular one's profession, are not taken into account in the operation of establishing equivalences that makes it possible to identify personalities or stars.
a surgeon and an explorer may be equivalent when they are considered from the standpoint of fame: "when a cosmonaut, a famous surgeon who has done successful heart transplants, or a celebrated explorer is featured at a company event, this is certain to attract the attention of the public".
however, arrangements of worth may include persons who are neither big names nor their jam but who serve as judges responsible for focusing attention on the worth established by fame.
this is the case, for example, with opinion leaders "whose opinion prevails and who determine public opinion," journalists who judge whether or not public opinion is "receptive" or not, public relations agents, hostesses, spokesperson, and press agents.
access to fame, which is potentially possible from one day to the next for any being, however lacking in resources it may be, can also be supported by an arrangement of objects.
to make oneself known, it is a good idea to have a name, or, for products, a brand name, inscribed on a medium such as a label or a badge.
communicating an opinion to the largest possible number of persons allows fame to spread by contagion; this is accomplished within an arrangement that includes a "sender, a receiver, and a medium, an intermediary charged with conveying the message to its intended audience".
rooted in the world of fame (and not in the industrial or market world), a business enterprise can be thus defined as a sender addressing a receiver: "the sender, that is, the company, and the receiver, that is, the public".
an effective arrangement, a good campaign that makes it possible to plant an image, thus presupposes "a medium that is perfectly adapted to a specific message and capable of putting this message in the best light".
the tools that help establish the worth of fame include brochures, flyers, booklets, magazines, newsletters, books, public relation offices, audiovisual productions, press releases, and interviews.
it is essential to "have the (company] leaders interviewed if they are well known; it is obvious that via a statement made or a position taken by mr. x, a big name, the company of which mr. x is president will be in the limelight".
as instruments for the worth of fame, objects that represent compromises with the industrial and civic worlds may be used, for example, public opinion surveys; by means of an industrial apparatus, these supply a "yardstick for opinion" or for the penetration of a message, that is, "the fraction expressed in terms of the percentage of a population reached by a given medium".
however, in the logic of public opinion, this yardstick is sought not merely for its own sake, but also for the way it helps spread the message: "the increasingly frequent recourse to public opinion surveys allows us to state the following fact: the publication of results indicating that a majority of persons has a given opinion reinforces the opinion of these persons, underwriting it, as it were, and influencing the opinion of others".
the regular publication of surveys thus contributes to ensuring the transparency of the state of worthiness of famous beings who cannot hide the fluctuations of their standing.
having no secrets is the price paid, more generally, for reaching the state of worthiness in this world.
to be known, one must agree to reveal everything, keep nothing back, hide nothing from one\'s public.
"the public has a real allergy to secrets".
stars thus give up not only their private lives but also individual or personal attitudes, which are denigrated in this world as extravagances or whims that might displease the public at large.
similarly, one must abstain from any "esotericism" (a mode of expression that has high value in the inspired world), for this mode is treated as a manifestation of ostracism with respect to the majority.
as a result, every act aimed at public opinion has to be carried out in relation to the least sophisticated sector, which implies that the same information must also be delivered to the most sophisticated sector".
in the world of public opinion, the relation of worth is a relation of identification.
the most worthy include the others because the latter identify with the former, as the fan identifies with the star.
but persons may also identify with objects that have been successful, and through these objects with the celebrities who have adopted and displayed them.
thus "every satisfied driver identifies with his car and \'defends\' it against judgments formulated by others".
to include others is to be recognized by others, to attract their attention, to convince them, to obtain respect from them, to earn or win their support.
a being of great fame creates a public, constitutes it as such, just as much as that being is created by its public.
the person that succeeds in breaking through, in capturing the attention of the public, includes and imparts reality to the being of those who, owing to the recognition that they grant that person, ensure his celebrity.
such a person "takes on the potential strength of each of his audiences" and manifests that strength by concentrating it in himself.
the term strength, although it is ambiguous, is used in the manual we are referring to here, as it is in hobbes's canonical texts, especially in the evocation of the way in which a famous person encompasses his public and increases in stature in proportion to its support.
the relation of inclusion among the worthy through their fame and their audience is expressed in terms of influence.
to establish a relation of influence, one has to hook, attract, alert, gain a following (fans) or a reputation, persuade, reach, sensitize, mobilize, interest, inform, seduce.
subjected to such influences, opinion creates fashion; it circulates the way a rumor does in a communications network, and thus imparts value to the beings of that nature whose fame spreads mutually from one to another: "if for example a company succeeds in inviting big names, stars, the reputation of those big names will carry over to the company itself".
being acquainted with something is equivalent to having heard of it: "by virtue of hearing of [someone or something] in any number of ways, the public has the impression, even if it is not involved in a consumer relationship, that it knows [that person or thing] .
talking about something, mentioning it, dropping a name, and publishing a book are all ways of "getting a message across," of releasing, spreading a piece of information.
in this world in which everything that has value is immediately known and visible, persons are constantly making comparisons.
thus, for example, "the press, in all its forms, allows employees of a company to compare their company, their own working conditions and salaries, to other companies in the same sector, or even in other sectors".
these crisscrossed comparisons weave a network.
persons who are receptive, receivers of the message, in tum become senders.
public relations offices are used to stimulate the process by creating "a network of free press agents".
in fact, those who have heard about something become its echoes, retransmit it, guaranteeing its repercussions; they convey information, they maintain the image, by multiplying and amplifying the message as a "center of resonance".
similarly, "a good public relations policy may make it possible, for example, owing to good contacts with the press, to amplify \'good information.\'" "a public" thus plays "a dual role: spectator and actor.
the public is a spectator when it receives certain information that may prompt a reaction of endorsement, opposition, or indifference.
but then the public becomes an actor, for in most cases it will talk about that information to other publics to which it can communicate its opinion".
one can intervene in this communications network, manipulate it in order to plant an image, launch or promote a product "information emanating from the press, personalities, opinion leaders" "appears in the public eye" and, by orienting and manipulating the image transmitted to the public, it positions the product.
the natural order distributes the range of images by positioning them in relation to their publics, which are divided up into targets or audiences.
in the world of fame, the peak moments are those during which these images become salient, for example during a presentation that places them in the spotlight under the gaze of others.
beings achieve worth only if the presentation is made visible, in a transparent space in which it can be looked at and compared.
the presentation may take the form of a press conference, "during which some important information is transmitted to journalists," or it may take the form of an inauguration or launching "that brings together big names, carefully chosen and important audiences, and journalists," and which makes it possible, while producing "as great a sensation as possible, ... to alert visitors and incite them to come in".
"a demonstration serves as a medium, from the vehicle to the message, obvious or implied, which makes it possible to achieve the desired goal.
consequently, the message may take on the form of a true message, it may communicate some information, or it may be only an ambiance, a climate that is created and that acts almost unconsciously on the audience present".
the brand name makes it possible to crystallize a trend in public opinion, through a brand image.
turning the press into an instrument ensures the objectification of this demonstration by its repercussions, the sensation that is produced: "the press is generally quick to echo the rumor and unconfirmed reports it picks up".
in the world of public opinion, judgment is in fact manifested by the convergence of opinions that produce a rumor, an unconfirmed report.
the demonstration is patent when this convergence is visible owing to the affluence of the world and can thus amplify itself on its own like a fashion: "it is well known that the world attracts the world and that the circus atmosphere (in the good sense) is always a safe bet".
the absence of judgment, in this world, consists in deluding oneself about one's own worth.
only a demonstration of the judgment of public opinion makes it possible to reduce the tension between the worth one attributes to oneself (ideal) and the worth that is attributed to one by others (real): "before undertaking any action at all on a brand image, it is important to know the real image (or the absence of a real image); to define with precision the ideal image that it would be desirable to promote".
to reduce a rumor or an unconfirmed report to its proper proportions, one must resort to the "reactions of public opinion".
the judgment may be supported, as we have already seen, by survey techniques that allow the establishment of a measure, a standing that helps reassure the celebrities of the undeniable worth that they are afforded by the recognition granted them.
indeed, in the world of public opinion, what is known is what is already obvious, and, conversely, what is either unknown to the majority (esoteric) or indistinguishable and lacking in relief is debatable.
to be unworthy, in the logic of public opinion, is to be banal (not to have been "debanalized"), "not to have any image at all, which in general signifies a complete ignorance of the product," or else to have an image that is fuzzy, degraded, faded, lost; to be forgotten or hidden, to encounter indifference or opposition, in short, to disappear": "certain companies struggle ... not to lose it [their image], not to disappear".
"all one has to do is keep a close eye on the press for a month to be persuaded of this: events that mobilize public opinion in a country for several days running are completely forgotten from one day to the next because they have disappeared from the newspapers.
