justifications based on the market [   market   worth]
arguments involving market justifications evaluate worth based on the price or economic value of goods and services in a competitive market.
relevant pieces of evidence brought in support of these arguments only qualify for market justifications as long as they can be treated as exchangeable goods or services.
these justifications consider the worth of things only in terms of price, and support a very short-term construction of time in which the market competition test is the basis for evaluation.
market arguments for the projects in dispute in our cases might include, for example, claims that the project will boost revenue for a region’s commercial areas, or that it is the cheapest method of providing a service for which there is demand.
in the french case, the road and tunnel were conceived and defended by the european community (ec) in brussels as an integral part of the pan-european transportation network intended to foster the free circulation of goods and people, which is the main reference point in the construction of the ec.
the road is defended as a way to reduce the cost of transit traffic.
the funding for the project was proposed in brussels as a way to promote competition and free markets in europe through better transport of goods.
tid maintained from 1990 through the end of 1994 that the dam was the cheapest way to meet rising electricity demand, while its opponents claimed other sources were clearly cheaper (and less environmentally damaging); both sides judged the project on market criteria.
in announcing the decision to shelve the project in january 1995, tid offered as its primary reasons the declining cost of natural gas and the impending deregulation of the electricity market which would make other sources more accessible. a different example of market justifications found in both cases involves the tourism and recreation industry, which is important to the regions in which both projects were proposed.
representatives of the tourism industry in the clavey area, led by an active group of river rafting companies and employees, repeatedly argue the dam would lead to a decrease in tourist dollars for the local economy.

the market world
adam smith\'s wealth of nations (primarily the first chapters describing how a market works) yields arguments basing a harmonious polity on the market.
the market link coordinates individuals through the mediation of scarce goods, the acquisition of which is pursued by everyone.
this competition among the individuals\' lusts subordinates to the desires of others the price attached to the possession of a commodity.
\'the calm desire for wealth\', as albert hirschman writes in the passions and the interest (hirschman, 1981), quoting francis hutcheson, allows the construction of a harmonious order which transcends the confusion of individual interests.
the market world must not be mixed up with the sphere of economic relations.
for our purpose, this book is of particular interest because it bases business success on an experience which is conspicuously unconnected to industrial production: the author made a fortune selling the names of famous people to advertising agencies.
in a market world, important persons are buyers and sellers.
they are worthy when they are rich.
their main qualities are those opportunistic in sorting and seizing the opportunities of the market, to be unhampered by any personal link and to be emotionally under control.
they connect with one another through competitive relationships.
competition (higher common principle) rivalry, competitors.
desirable (state of worthiness) value (of), millionaire, winner.
interest (dignity} love (of things), desire, selfishness competitors (subjects) businessman, salesman, client, buyer, independent (worker).
wealth (objects) luxury (item).
opportunism (investment) liberty, opening, attention to others, sympathy, detachment, distance (emotional), perspective (getting some).
possess (relation of worth) interest (to) (relations} buy, get (for oneself), sell, business (do business with), negotiate, benefit (from), market, pay, compete. the market world must not be confused with a sphere of economic relations.
we have tried to show, on the contrary, that economic actions are based on at least two main forms of coordination, one by the marketplace, the other by an industrial order, and that each has its own way of setting up a reality test.
such problems surface in particular when time is introduced as a factor in market relations, which are atemporal by nature.
as for the market world, it is not animated exclusively by business relations between buyers and sellers.
this world is also peopled with omnipresent objects; the economist can overlook their role in the coordination of actions only when he treats them as endowed with a nature independent of the interventions of others.
in this he displays the attitude of a person acting normally in the market world; in order to reach agreement on a transaction, such a person has to rely on the objectivity of the item and on its independence with respect to the various persons implicated in the transaction.
by bringing to light the quality of objects of a market nature and their role in coordination, we are preparing to deal with the complex situations in which ambiguous objects disturb this coordination: something scribbled by picasso on the corner of a table, a dented tank that does not meet industrial contemporary standards, a used car, and so on.
in this paragraph, we are pursuing the demonstration we undertook in our analysis of the market polity, by indicating how the market world can be related to the same model of worth, and what specifications are established in it for relevant beings and relationships.
in the market world, actions are motivated by the desires of individuals, which drive them to possess the same objects, rare goods whose ownership is inalienable.
the characterization of this world by the dignity of persons, all equally motivated by desires, and by the adequacy of the objects that equip it already encompasses the principle of coordination, competition, which can be made explicit in the justifications to which tests give rise.
viewed from the outside, the constitutive convention of competition plays the same role as the conventions that serve as higher common principles in the other worlds.
the construction of the market is neither more nor less a metaphysics than the construction of the orders that refer to trust and tradition or to the general will.
the competition between beings placed in a state of rivalry governs their conflicts through an evaluation of market worth, the price, which expresses the importance of converging desires.
worthy objects are salable goods that have a strong position in a market.
worthy persons are rich, millionaires, and they live the high life.
their wealth allows them to have what others want, valuable objects, luxury items, upscale products.
their wealth is proportionally to their own value, which they know how to sell, and which is expressed by their success, designated in particular by the vocabulary of competition: getting ahead, challenging oneself, taking the edge, being a winner, a top dog.
the deployment of market worth is inscribed in a space that has neither limits nor distance, in which the circulation of goods and persons is free.
businessmen and women think big, oversee world markets, and do international deals throughout the world.
market worth does not participate in a construction of time.
the state of worthiness includes no memory of the past, no plan for the future.
in business, one gets ahead by leaping to the top, just as one unexpectedly goes bankrupt.
instability does not imply a defect, as it does in the industrial world.
luck may go the wrong way, but one can take advantage of insecurity.
fate can usually be made to tum out in one\'s favor, transformed into good luck, if people exploit the situation opportunistically and take advantage of opportunities that arise.
the state of unworthiness is one in which persons fail, stagnate, and lose out, and in which goods are rejected, spurned, hated, instead of desired.
lacking any means of buying or selling, poor persons come close to escaping the convention of the common good and to being deprived of the dignity of human beings in this world.
the human nature that flourishes in the market world is characterized by a desire as innocent as any dignity.
"go for profit. samuel johnson once said: \'there are few ways in which a man can be more innocently employed than in getting money\'"
this capacity is inherent in everyone: "most people, i believe, are born salesmen"
it precedes consciousness itself: "[the art of selling is the conscious practice of a lot of things we already know unconsciously- and have probably spent the better part of our lives doing" (89).
one succeeds through the strength of this desire, because one loves.
real life is what people want to acquire.
in the manual we are using, the author recounts a scene in which the participants are playing with the limits of humanity and having fun by attributing to nonhuman beings this dignity of desire, this capacity to love or to hate.
at an annual sales convention, the president of a dog food company summed up the situation this way: "\'over the past few days,\' he began, \'we\'ve heard from all our division heads and of their wonderful plans for the coming year. if we have the best advertising, the best marketing, the best sales force, how come we sell less goddamn dog food than anyone else in the business?\' absolute silence filled the convention hall. finally, after what seemed like forever, a small voice answered from the back of the room: \'because the dogs hate it\'" (117).
since dignity designates a capacity to participate in a common good, the fact that it takes the form of a selfish desire in the market world is almost paradoxical.
by recognizing dignity in persons occupied with satisfying their self-centered desires, utilitarian philosophy has contributed to establishing the modem image of the individual detached from the chains of belonging and liberated from the weight of hierarchies.
however, the success of the figure of the individual has been largely dependent on proof that individuals can find a place in an order in which their actions are coordinated with those of others.
political philosophies have demonstrated the benefits of competition only by establishing this possibility of coordination, which makes it possible, today, to refer to a market polity.
only if one loses sight of the convention that creates bonds between people as individuals- a convention that operates via their desire for the same objects-and if one forgets the way in which this convention of competition governs its own free play, do the terms "individual" or "freedom" become susceptible to the semantic slippages that lead to the presentation of the market world as the sole guarantor of people\'s autonomy and freedom.
the market world is thus populated with individuals seeking to satisfy desires; they are in turn clients_ and competitors, buyers, and sellers, entering into relations with one another as businessmen.
the fact that the deployment of objects is required for market coordination becomes obvious, in contrast, when the identity of market objects is lacking.
an object of a market nature is a thing toward which competing desires for possession converge: it is a desirable, salable, marketable thing.
the identification that is common to market objects inscribes them as inalienable property, objects that are common to diverse desires.
let us note that of all the social sciences, economics is the one that grants the most space to objects, although the status of these objects is not made clear in economic theory.
market goods are so natural that their role is rarely addressed, apart from considerations about public or collective goods.
recent theoretical developments regarding the quality of goods do touch on the crucial hypothesis of a common identification of objects, but the problems raised are reduced to a matter of asymmetrical information.
the value of the item is determined in a market test, and problems dealt with as instances of cheating on real quality are raised by the implicit reference to a different form of value, often industrial in nature (eymard duvernay 1989b, p. 12 7).
similarly, the implications of a theoretical slippage from a market of goods to a market of contracts are not clearly distinguished, since the role of the hypothesis of the objectivity of a medium for the expression of competing desires is not taken into account.
the common identification of objects of a market nature is closely linked to the requirements of aggrandizement in a market world, to the investment formula that ensures access to the common good by means of a sacrifice.
the selfishness of subjects in the market world has to go hand in hand, if any order is to result from their competition, with clear-sightedness as to the limits of selves; this is the other side of the picture, where the common identification of external goods is concerned.
this requirement, often forgotten in methodological or political approaches to individualism, is the one that defines the individual as a social being, socialized by desires that converge toward external goods.
in the market world, people are thus detached from one another (and in particular from any domestic bond), liberated in such a way that they lend themselves willingly to every opportunity to engage in a transaction.
in short, subjects are as available as goods on the market.
here, too, the adoption of this approach to the market world makes it possible to dispel ambiguities about freedom and liberalism.
it might be preferable to recognize that the freedom of free-market liberalism has meaning only insofar as it is an expression of choice for external goods, and that this externality is acquired only if one starts out with a detachment that presupposes seeing others the way one sees oneself.
the opportunism that characterizes the worthy those who know how to make the most of everything, in the market world- thus goes hand in hand, unparadoxically, with a certain attention to others that presupposes "the ability to listen, really to hear what someone is saying" (mccormack 1984, 8) (inasmuch as others are detached individuals themselves, and not inasmuch as they form an opinion from which one has to protect oneself, like "the others" in the world of fame).
this attention does not serve to temper selfishness; it is consubstantial with selfishness in the market order.
just as the work by adam smith from which we extracted the canonical presentation of market worth invites us to associate a theory of the wealth of nations foregrounding the principle of competition, on the one hand, with a theory of moral sentiments based on the position of impartial spectator and sympathy, on the other, the manual we are using here makes visible the relation between the satisfactory progress of business and a state of persons characterized by detachment with regard to themselves (a distance that recalls the "equal distances with respect to great and small objects" at which smith positioned_ the spectator) and by attention to others.
even if the market world is not reduced to a collection of atoms deprived of connections with one another market relations propagated in networks can easily be contrasted with hierarchical relations embedded one within another.
and yet the market order also espouses the order of the general and the particular that implies such embedding.
market worth is not so very different from the worths that serve as examples for the notion of hierarchy, the domestic order of authority, or the industrial order of competence.
since market objects encompass the desires of others, their possession implies a hierarchical relation in the ordinary sense of the term.
the state of worthiness encompasses the state of unworthiness in a relation of possession.
the price of an object is the proof of the attachment of others to the good that one is holding.
since not all persons can satisfy their appetites equally by acceding to the same rare goods, the wealthiest incorporate the others by possessing the desire of those who are less wealthy and who remain deprived of goods.
millionaires are defined by their possession of things everyone wants.
when they are engaged, the beings of the market world are doing business.
business consists of at least two individuals plus an object whose purchase and sale they negotiate.
the object, a good or a service, helps to fashion a link between people by attracting, by interesting.
the transaction presupposes that people have enough perspective and that the object is detached enough to allow the play of competition with others.
local transactions thus exceed settlements and become a market that takes into account the entire set of the others\' desires through the prices that will be negotiated and paid.
the assemblages are coherent when the market beings are of the same worth, when the product is well positioned in relation to the buyer and the seller.
this operation takes into account the reality of the buyer\'s desire for the object.
at the end of this harmonious process of composition, the persons doing business find their worth naturally in relation to the object negotiated.
being in a business deal that holds together requires persons who meet, often face to face, to negotiate head to head, far from the influence of others; they must take one another\'s measure accurately and "never underestimate [the] competition" (205).
the harmony of the natural order stems from the way in which goods acquire their price in a market that determines the distribution of states of worth.
the test is the moment, uncertain in its outcome, when a deal is done, settled, in the bag.
the end of the test is expressed by the signing of a contract.
the test is also an opportunity to extract new objects from chaos, to discover new things that might interest a client and give rise to a transaction and opportunity to extend the market world.
the common identification of goods and the generality of the worth known as price associates the face-to-face aspect of a business deal with other transactions taking place elsewhere, with other individuals; these can thus be equated.
price, which sanctions the test by modifying the distribution of worths, takes into account the negotiation between the two subjects involved.
but this takes place on the horizon of a general price, a requirement expressed in the fact that the price has to be reasonable and must correspond to the real value.
the generality of the price is ensured by a monetary standard.
money is the measure of all things, and thus constitutes the form of evidence.
the profit, the benefit, the payback, the result of the transaction is thus expressed in cash, a commission, a fee, or an honorarium.
this place assigned to money in market world tests does not allow us to settle any debates over monetary theory, of course, but it suggests that money, in its function of reserve value attached to a projection onto the future, is an ambiguous entity that opens up a passageway to other worlds.
the inhuman limit of the market world is marked in a long tradition criticizing the vanity of the possession of riches and advocating an attitude of wise detachment toward material wealth.
in distinguishing between goods and a person\'s other qualities, seneca- unlike hume or smith-denied that the former expressed the latter in any way and that they might serve to justify any sort of worth whatsoever: "i own my riches, yours own you .. .
wealth can lead, in a confusion between persons and things, to the direct possession of another person rather than of the goods that that person may desire.
price (judgment} value (justified, reasonable, real).
money (evidence) benefit, result, payback enslavement to money (the fall)