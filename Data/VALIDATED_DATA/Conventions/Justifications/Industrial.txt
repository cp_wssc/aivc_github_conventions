justifications based on technical efficiency and planning [   industrial   worth]
another category of justifications includes arguments where evaluations depend on technical efficiency and professionalism, planning, and longterm investment in infrastructure.
this category relates to the industrial order of worth in the boltanski and thevenot (1991) scheme, but industrial here is not limited to the industrial economic sector.
while technical competency and planning arguments are sometimes connected as such to economic outcomes, the bases for evaluation in this category are different from market criteria.
technical competency justifications place value based on the efficiency of investments, professional planning and expertise, and long-term growth.
the form of proof for planning justifications is long-term investment and technical or scientific competency. 
they envision nature as a scarce economic resource with multiple potential uses energy, recreation, income for the county which must be used efficiently.
also, both sides utilize a form of proof which is congruent with technical competency justifications the use of scientific expertise and evidence to validate opposite arguments, for example on the question of whether the dam will hurt or help the clavey fish populations.
the project is defended because of the necessity of providing roads, tunnels, and other infrastructure in order for there to be economic growth in the future. this sort of industrial planning argument is extremely influential as an argument for the tunnel project in france, and reflects the well-documented embrace of an engineering mentality or technocratic approach by the french state.
this strong compromise of technical industrial justifications closely connects technocratic planning by the state with the general interest, and this approach is perpetuated through the training of elites in engineering schools for high positions in the state..
they are both based on the same type of test the long-term efficiency of the project with the same criteria of proof and instruments of evidence (technical evidence from modeling and statistical analysis).
for example, hydropower projects in france were were judged based on their efficiency as long-term investments. in the industrial city, for example, the common superior principle is efficiency.
it can be said: \'in terms of efficiency, x is equivalent to y\'.
similarly, using this same convention, it can be said that \'z is superior or inferior to x\'.
thus, identification of the common superior principle in a city leads us straight to the <condition of great man>, the great man being the one who embodies the city\'s values, as well as to the <condition of little person>, defined by the absence of such values.
thus, someone situated in an \'industrial\' world will readily refer    to \'instruments\', \'methods\', \'measures\' or \'procedures\', invoke \'engineers\' and \'specialists\', and list \'controlling\' or \'organizing\' as among the actions worth performing.
the lndustrial world
the industrial principle of equivalence was distilled from the work of saint simon, founder of french sociology.
in this world, worth is based on efficiency.
lt can be measured on a scale of professional capabilities.
connected to the production of material goods, industrial worth is upheld by way of organizational devices directed towards future planning and investment.
to describe the objects of the industrial world we used a productivity guide, productivite et conditions de travail.
in an industrial world the great persons are the experts.
the words used to describe their personal qualities can also be used to qualify things.
they are said to be worthy when they are efficient, productive, operational.
they implement tools, methods, criteria, plans, figures, graphs, etc.
their relationships can be said to be harmonious when organized, measurable, functional, standardized. the industrial world is the one in which technological objects and scientific methods have their place.
the terminology adopted must thus not lead to the assumption that this world is fully inscribed within the limits of industry.
conversely, the workings of an industrial enterprise cannot be understood on the basis of resources stemming from this world alone, even if the aim of efficient production based on functional investments finds its justification in the industrial order.
if, as we shall see, a canonical judgment as to the quality of a scientific phenomenon allows us to illustrate the reality test of the industrial world, the fact remains that the development and diffusion of a discovery cannot be justified simply by the test examined here; these phenomena exceed the framework of the industrial world by a wide margin.
works on the history or the sociology of science and technology bring out the heterogeneity of the resources and actions involved in the processes of innovation, and they are even joined today, on this point, by economic approaches to technological change.
certain actions are inscribed in an industrial test where the intent is to establish scientific proof.
others are implicated in the peak moment when a singular phenomenon emerges, an innovation announcing a break with tradition according to an inspired justification.
still others rely on the worth of fame, a worth that implies a concentration of credit in terms of public opinion based on distinctive signs and marks; others depend on the venerability of domestic bonds guaranteeing a solid reputation or on the establishment of market value in an immediate response to clients\' desires.
just as the recognition of the market order is obscured by a misunderstanding of the conventions that accompany the affirmation of an individual, the understanding of the industrial order is blocked by a treatment of technological objects that locks them into an instrumental relation to nature and fails to examine the conventions supporting agreement about the scientific phenomenon in question.
the observation of the conditions under which industrial objects are put to work shows that they lend themselves to the same type of test as the one already identified in the other universes.
testing these beings and establishing their objectivity presupposes a detour through a collective form, a higher common principle that governs the way they are judged.
by highlighting this industrial test, we are distancing ourselves both from an approach in which science and technology are openings onto an external world, and from a radical relativism that construes facts as arbitrary beliefs attached to communities.
the ordering of the industrial world is based on the efficiency of beings, their performance, their productivity, and their capacity to ensure normal operations and to respond usefully to needs.
this functionality is expressed in an organization, and it implies both a synchronic articulation with other beings and a temporal articulation.
efficiency is in fact inscribed in a regular bond between cause and effect.
the proper functioning of beings extends the present into a future, opening up the possibility of prediction.
the industrial form of coordination thus supports an equivalence between present situations and situations to come, and constitutes a temporality.
tomorrow is what counts: the "machines of tomorrow," the "worker of tomorrow," "the organization of tomorrow."
the quality of worthy beings, beings that are functional, operational, or (when humans are involved) professional, thus expresses their capacity to integrate themselves into the machinery,     , along with their predictability, their reliability, and it guarantees realistic projects in the future.\npeople are in a state of unworthiness when they produce nothing useful, when they are unproductive, when they fail to do much work, owing to absenteeism or turnover, or because they are inactive, unemployed, handicapped, or when they turn out work of poor quality- because they are inefficient, unmotivated, unqualified, unsuited to the job. things are unworthy when they are subjective.
beings are also unworthy when, instead of being open to the future, they cling to the mold of the past, by failing to evolve, by remaining static, rigid, ill-adapted.\n (especially waste of "human capabilities" resulting from "unqualified work that ... does not correspond to the real capabilities"), spoilage, rejects, pollution, deterioration, these are all negative signs of worth; they manifest weak control, the poor functioning of a disturbed system ("the quality of the raw material is uneven and disrupts production"), and they originate in random events, incidents, and risks.
the questioning of the industrial order is expressed in a nonoptimal situation, as when one "observes that the programming of production is not optimizing the costs."
this contentious situation is a malfunctioning, a problem, a breakdown, an accident: "the reduction of rejects, accidents, and nonproductive time often makes it possible to reduce physical costs and wastes of human energy" at the same time.
the dignity of persons, the aspect of human nature on which the industrial order is based, lies in the human potential for activity.
this capacity is expressed in work that is the implementation of the energy of men of action.
"to invest in human capabilities and energies is to use the best means for economic efficiency."
consequently, the failure to use the available human potential is a serious breach of human dignity.
in the industrial world, people have a professional qualification (the term professional is used, moreover, as a substantive to designate them) related to their capability and their activity.
this scale of qualification underlies a hierarchy of states of worth, a hierarchy marked by competencies and responsibilities (management, leaders, decision makers, supervisors, technicians, operators, and so on). in work relations and systems of remuneration, the formal qualities that express industrial worth are opposed both to a market evaluation that would result immediately from a service provided and to a domestic judgment that would evaluate a person\'s authority.
the objects in the industrial world are instruments, means, mobilized for an action that is presented as a task of production.
this instrumental construction of action, implying a spatial and temporal detour by way of objects that serve as relays, is thus envisaged here as a specific feature of the industrial world and not as a property that would be of a higher order of generality characterizing the actions of human beings endowed with reason.
production activities are not organized in the same world as the actions of a market nature analyzed earlier, contrary to what is implied by the reduction of the productive function brought about in the economic theory of general equilibrium.
production is carried out in a deployment of objects of an industrial nature that is extended from tools to procedures.
the manufacturing of products brings into play raw materials plus energy, machines, and methods: "a whole panoply of tools, organizations, methods and methodologies must be available so that at every moment the best available equipment can be used and then set aside as soon as it is no longer appropriate."
the human body is the primary tool that works through effort, and objects of an industrial nature are merely instruments that extend the efficiency of the body\'s work.
the economic theory of capital converges with the sociology of science and technology when it uses military metaphors to describe "enlisting ... potent forces as our allies in the task of production."
the coherent assembly of these objects sustains a causality inscribed within a temporality: once the arrangement is in place, only a modest gesture is required to unleash a series of significant effects.
objects of an industrial nature contribute to shaping a space in which effects are transported by means of mechanisms.
space is organized in such a way that distant zones, or zones unrelated to the action, according to a domestic topography, are treated as an environment as soon as functional links have been established.
the various actions are integrated into a single homogeneous plan which is governed by axes, guidelines, dimensions, degrees, and levels.
objects are related to one another within this space with the help of lists and inventories dealt with by lot.
measurable space can be projected on a piece of paper where part of the test is played out, thanks to the construction of grids, states, graphs, charts, flow charts, frameworks, accounts, indicators.
the spatial articulation of objects presupposes a capacity to define that is equipped with a measuring instrument.
the measuring instruments used are actually machines that are incorporated into the workspace.
they standardize by using definitions and investigations to produce objects in due form whose functions can be grasped by criteria or characteristics.
even human bodies can be caught up in these measures and inscribed in the ergonomics of the task to be carried out: "ergonomics can help us determine the physical cost of a workstation and express it in kilocalories per day ....
the stations have been modified (height, position, seats) as a function of the average measurements of segments of the personnel."
function is a notion that must be understood in a spatial articulation and a temporal liaison, as mechanical devices illustrate.
the temporal equivalence instituted by industrial worth is particularly visible in objects apprehended according to their aptitude for managing the future, such as flow charts, plans, and budgets: "by making adjustments to the planning calendar, we are integrating the project into the plan, the working and investment budgets."
the tools for measuring time take advantage of the regularity with which industrial objects function and endow the industrial world with a representation of time that is not unlike the topography of this world, in which one can be transported without friction, can move ahead or make a retrospective return to the past.
calendars and timetables make it possible to establish states of periodic advancement, to trace stages, phases, deadlines.
this shaping of time models in return the reference points for action constituted by goals and missions.
the analysis of the objects of the industrial world makes it possible to understand the possibility of calculations, by reversing the perspective adopted when one reduces calculations to the exercise of a mental faculty belonging to beings endowed with reason.
the instruments for defining and measuring constitute the situation of action as a problem leading to the formulation of hypotheses and calling for a solution.
the articulation of elements or segments obtained by the decomposition of the complexity of the universe can be accomplished by mathematical operations, since the calculations are based on quantified variables: "the inventory of problems and their alternative solutions is subjected to the method of economic evaluation that makes it possible to quantify the various hypotheses for improvement."
progress is the investment formula in the industrial world.
it is associated with the operation of investment (in the classical sense of the term) that weighs the "price of efforts," "heavy in time and in money," and the "middle term profitability" that they ensure: "the investments will open the way to new development."
industrial worth requires this dynamic in order to avoid obsolescence, the "future obsolescence of the existing organization."
the temporal orientation is constructed on the basis of the future (as is seen in investment decisions or in arguments for optimization by "backward induction"), unlike the temporal orientation that prevails in the domestic order, an orientation generated by the past.
let us note that the calculation of the profitability of an investment, as soon as it takes an interest rate into account, integrates the constraints of a financial market that is not inscribed in the industrial order.
it is in a relation of control that the state of worthiness encompasses the state of unworthiness.
the word responsibility may be ambiguous here, because it also serves to designate the relation of domestic worth.
however, the industrial responsibility of the worthy person does not imply that he has power over a less worthy person who owes him respect in return.
the control one exercises depends only on the possibility of predicting less complex actions by integrating them into a larger overall plan.
a more worthy person is in relation with a less worthy person primarily through the "responsibility he assumes" for production, by the control he has over the future: "to determine the future accurately in order to control it is an indispensable task of factory management."
natural relations are those required by the regular functioning of beings of an industrial nature.
they are thus dependent on the qualities of the objects already identified. in the first place, they put to work production factors organized in structures or systems made up of adjusted mechanisms, adapted machinery, and interactions.
the functional connections are established in the mode of the necessary, the required.
these inexorable, indispensable requirements take the form of constraints that condition action and must therefore be taken into account; one must take responsibility for them.
persons themselves are integrated as a function of the more or less complex competencies they exercise: "each hierarchical level takes upon itself what cannot be delegated to a lower level; in exchange, it has access to efficient tools for control."
the functional relations of the industrial world share in a temporal stability that is favorable to forecasting: "the production unit appears on the whole as a rather well stabilized universe that is very well controlled."
the organization of the beings installed in an industrial arrangement goes hand in hand with an organization of its functioning, with stabilization procedures that, in production, work primarily through stocking operations.
the implantation of an industrial arrangement presupposes adjustments in the environment, adaptations, redefinitions: "the overall program is adapted to each particular terrain and all its phases are redefined as a function of the characteristics proper to this terrain, while the overall framework is maintained."
industrial action requires a correct vision of this space in which the problem is inscribed, so as to detect, discover, identify, bring to light, measure, analyze, and decompose the relevant elements.
similarly, the results of the action taken are apprehended through their traces on this space.
from space to class, the code breaks a path.
the operations of standardization and formalization make it possible to see the world through data expressed in numbers, quantified, ready to be processed, combined, added up.
the description of the constitutive elements of the time and space proper to the industrial world makes it quite clear that the operation of optimization is possible only in an environment of beings of an industrial nature.
rational optimization is not situated in continuity with the immediate satisfaction of a desire, in the extension of a market coordination.
in fact, the temporal extension of the criterion of optimization in economic theory, which passes through an optimization of expectation of the anticipated utility, confronts difficulties that result from the introduction of irreversibilities inherent in the investment decision. the harmony of the industrial order is expressed in the organization of a system, a structure in which each being has its function, in short, a "technically predictable universe": "no blatant contingent malfunctions are visible; all the cogwheels of the organization work together without a hitch."
however, the equilibrium is not static (which would lead to a rapidly outdated state) but dynamic; the equilibrium is one of growth, expansion.
the testing of this organization presupposes verifying that things function as predicted, that the solution to the problem is realistic.
once the decision is made, the arrangement set up, the project launched, the mechanism started, its proper operation can be judged by an evaluation of its performance on the basis of the effects produced.
it remains to be seen whether the arrangement functions correctly, whether everything is in working order, whether "it\'s going well."
this functioning may lead to a reordering of the hierarchy of functions: "the qualification of the tasks evolves in the opposite direction from the hierarchy of the original functions.
the preparation is gradually disqualified owing to automatization."
the test is also an opportunity to reveal new objects.
the measuring devices mentioned earlier contribute to the process of revelation by achieving spatial and temporal equivalencies, by carrying out operations in series.
they make it possible to establish laws on the basis of frequencies, and to reduce the role of chance by exhibiting probable relations on the basis of averages.
proof is grounded in a temporal regularity, the methodic repetition of measurement.
new causes, new factors capable of giving rise to effects, are thus identifiable during tests.\nin the industrial world, the distinctive dignity of humanity is threatened by the treatment of people as things.
the worth of the objects and arrangements created can be confused with this dignity to the point of blurring the boundary of humanity.
deprived of tests, protected from the risk of contingencies that can call the order of competencies into question and allow new objects to emerge, industrial worth can become frozen in the monumental order that is regularly depicted in critiques of technology.\n\nefficiency (higher common principle)\nperforr,umce, future efficient\nreliable,\nnot\noptimal, inactive,\nunsuited, breakdown (in a state of),\nunreliable.\nwork (digni1y}\nenergy.\nprofessionals\n(subjects) expert,\nspecialist, person in charge, operator.\nmeans (objects) tool, resource, method,\ntask, space,\nenvironment, axis,\ndirection, dimension,\ncriterion, definition,\nlist, graph, chart,\ncalendar, plan, goal,\nquantity, variable,\nseries, average,\nprobability, standard,\nfactor, cause\nprogress\n(invfsiment)\ninvestment, dynamic.\ncontrol \nfunction\n(relationships) put to work, machinery (liaison of), function (to be a function of),\ncogwheels, interact,\nneed (to),\ncondition(to),\nnecessary (relation),\nintegrate, organize,\ncontrol, stabilize,\norder, anticipate,\nimplant, adapt,\ndetect, analyze,\naccount (take into),\ndetermine, light\n(bring to), measure,\nformalize,\nstandardize,\noptimize, solve,\nprocess.\norganization\n(figures) system.\ntrial (test)\nlaunching, setting up,\nputting to work,\nachievement.\neffective (judgment)\ncorrect, in working order, functioning.\nmeasure (evidence)\ninstrumental action (the fall) treating people as things.\n'
